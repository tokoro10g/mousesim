#include <iostream>
#include <vector>
#include <stdint.h>

#include "graph.hpp"
#include "maze.hpp"

#ifndef _VIEWER_HPP_
#define _VIEWER_HPP_

class Viewer{
	private:
		Maze *maze;
		Route route;
		Coord c;
		static const char *DIRCHR;
	public:
		Viewer();
		Viewer(const Maze& _maze);
		Viewer(const Maze& _maze,const Route& _route);
		Viewer(const Maze& _maze,const Route& _route,const Coord& _c);
		~Viewer(){ delete maze; }

		void append(const Maze& _maze);
		void append(const Route& _route);
		void append(const Coord& _c);
		void clear_maze();
		void clear_route();
		void clear_coord();
		void show() const;
};

#endif
