#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <stdio.h>
#include <stdint.h>
#include "graph.hpp"
#include "maze.hpp"
#include "mouse.hpp"
#include "viewer.hpp"
#include "stopwatch.hpp"

int main(){
	int goal=135;
	int start=240;

	int type,maze_w,maze_h;
	std::cout<<"Input Maze Data:"<<std::endl;
	std::cin>>type>>maze_w>>maze_h;

	Maze realMaze(maze_w);
	Maze maze(maze_w);

	for(int i=0;i<maze_h;i++){
		for(int j=0;j<maze_w;j++){
			char chr;
			CellData cell={{0},{0},{0},{0}};
			std::cin>>chr;
			if(chr>='0'&&chr<='9'){
				cell.wall.half=chr-'0';
			} else if(chr>='a'&&chr<='f'){
				cell.wall.half=chr+0xa-'a';
			} else {
				std::cerr<<"Invalid maze data"<<std::endl;
				return 1;
			}
			realMaze.addCell(cell);

			cell.wall.half=0;
			if(j==0&&i==maze_h-1)
				cell.wall.bits.EAST=1;
			if(j==1&&i==maze_h-1)
				cell.wall.bits.WEST=1;
			if(i==0)
				cell.wall.bits.NORTH=1;
			if(j==0)
				cell.wall.bits.WEST=1;
			if(j==maze_w-1)
				cell.wall.bits.EAST=1;
			if(i==maze_h-1)
				cell.wall.bits.SOUTH=1;
			if(i*maze_w+j==goal)
				cell.cflag.bits.END=1;
			if(i*maze_w+j==start)
				cell.cflag.bits.START=1;
			maze.addCell(cell);
		}
	}

	Viewer *viewer=new Viewer(realMaze);
	viewer->show();

	Mouse *mouse=new Mouse(maze,maze_w);

	mouse->searchMaze(viewer,realMaze,maze_w,start,goal,true);
	std::vector<int> indexes;
	mouse->getUnsearchedCellIndexesOnEstimatedShortestPath(indexes,start,goal);
	while(indexes.size()>0){
		int index=indexes.front();
		mouse->searchMaze(viewer,realMaze,maze_w,mouse->getCoordIndex(),index,true);
		mouse->getUnsearchedCellIndexesOnEstimatedShortestPath(indexes,start,goal);
	}
	mouse->searchMaze(viewer,realMaze,maze_w,mouse->getCoordIndex(),start,true);
	Route solvedRoute=mouse->searchMaze(viewer,realMaze,maze_w,start,goal,false);

	std::cout<<std::endl<<std::endl<<"Solved Maze:"<<std::endl;
	viewer->clear_maze();
	viewer->clear_coord();
	viewer->clear_route();
	viewer->append(realMaze);
	viewer->append(solvedRoute);
	viewer->show();

	std::cout<<"Solved Route:"<<std::endl;
	for(Route::iterator it=solvedRoute.begin();it!=solvedRoute.end();it++){
		std::cout<<*it<<std::endl;
	}

	delete mouse;
	delete viewer;

	return 0;
}

