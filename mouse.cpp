#include <iostream>
#include <vector>
#include <unistd.h>
#include "mouse.hpp"
#include "stopwatch.hpp"

const long Mouse::STEP_TIME;
const char* Mouse::dirchr=" ^> v   <";

Mouse::Mouse(const Maze& newmaze,int width){
	w=width;
	c.dir.half=1;
	c.x=0;
	c.y=w-1;
	maze=new Maze(newmaze);
}

void Mouse::senseAndSetWall(const Maze& realMaze){
	if(senseRight(realMaze)){
		//std::cout<<"right,";
		setWall(transDirection(Maze::DirRight));
	}
	if(senseLeft(realMaze)){
		//std::cout<<"left,";
		setWall(transDirection(Maze::DirLeft));
	}
	if(senseFront(realMaze)){
		//std::cout<<"front,";
		setWall(transDirection(Maze::DirFront));
	}
}

bool Mouse::senseFront(const Maze& realMaze) const{
	Direction dir={1};
	maze->setChkWall(c,transDirection(dir));
	return realMaze.getMazeData()[c.y*w+c.x].wall.half&transDirection(dir).half;
}
bool Mouse::senseRight(const Maze& realMaze) const{
	Direction dir={2};
	maze->setChkWall(c,transDirection(dir));
	return realMaze.getMazeData()[c.y*w+c.x].wall.half&transDirection(dir).half;
}
bool Mouse::senseLeft(const Maze& realMaze) const{
	Direction dir={8};
	maze->setChkWall(c,transDirection(dir));
	return realMaze.getMazeData()[c.y*w+c.x].wall.half&transDirection(dir).half;
}
bool Mouse::moveForward(){
	c.x+=!!(c.dir.bits.EAST)-!!(c.dir.bits.WEST);
	c.y+=!!(c.dir.bits.SOUTH)-!!(c.dir.bits.NORTH);
	return true;
}
void Mouse::turnRight(){
	c.dir.half=(c.dir.half<<1)&0xF;
	if(c.dir.half==0) c.dir.half=0x1;
}
void Mouse::turnLeft(){
	c.dir.half=(c.dir.half>>1)&0xF;
	if(c.dir.half==0) c.dir.half=0x8;
}

void Mouse::setMaze(const Maze& newmaze){
	delete maze;
	maze=new Maze(newmaze);
}
void Mouse::clearMaze(){
	int width=maze->getWidth();
	delete maze;
	maze=new Maze(width);
}

void Mouse::setWall(Direction dir){
	maze->setWall(c,dir);
}

Direction Mouse::transDirection(const Direction local) const{
	Direction global=c.dir;
	Direction dir={0};
	if(local.bits.NORTH){
		return global;
	} else if(local.bits.EAST){
		dir.half=(((global.half&0x8)>>3)|(global.half<<1))&0xF;
		return dir;
	} else if(local.bits.WEST){
		dir.half=(((global.half&0x1)<<3)|(global.half>>1))&0xF;
		return dir;
	} else {
		std::cout<<"ouch"<<std::endl;
		return dir;
	}
}

const Route Mouse::searchMaze(Viewer* viewer,Maze& realMaze,int maze_w,int start,int goal,bool isSearching){
	int index;
	Route route;
	StopWatch sw;

	while((index=getCoordIndex())!=goal){
		Coord coord=getCoord();

		sw.start();
		Graph graph(getMaze());
		sw.stop();
		std::cout<<std::endl;
		std::cout<<"********************"<<std::endl<<"Graph Generation:";
		sw.show();

		sw.start();
		graph.dijkstra(goal,index,isSearching);
		route=graph.getRoute(goal,index);
		sw.stop();
		std::cout<<"********************"<<std::endl<<"Dijkstra Method:";
		sw.show();
		std::cout<<"********************"<<std::endl;
		for(Route::iterator it=route.begin()+1;it!=route.end();it++){
			std::cout<<std::endl<<index<<"->"<<(*it)<<std::endl;

			Direction routeDir={0};

			if((*it)==index-maze_w){	//Route: NORTH
				routeDir.half=1;
			} else if((*it)==index+1){	//Route: EAST
				routeDir.half=2;
			} else if((*it)==index+maze_w){	//Route: SOUTH
				routeDir.half=4;
			} else if((*it)==index-1){	//Route: WEST
				routeDir.half=8;
			} else {
				std::cerr<<"wrong path. break."<<std::endl;
				return Route();
			}

			senseAndSetWall(realMaze);

			//std::cout<<index<<":";
			//std::cout<<std::endl;
			std::cout<<"route:"<<routeDir.half<<"front:"<<transDirection(Maze::DirFront).half<<std::endl;

			viewer->append(route);
			viewer->append(getMaze());
			viewer->append(getCoord());
			viewer->show();

			if(routeDir.half==transDirection(Maze::DirFront).half){
				std::cout<<"route:front"<<std::endl;
				if(senseFront(realMaze)){
					std::cout<<"sensor detected:front"<<std::endl;
					break;
				} else {
					moveForward();
					usleep(STEP_TIME);
				}
			} else if(routeDir.half==transDirection(Maze::DirRight).half){
				std::cout<<"route:right"<<std::endl;
				if(senseRight(realMaze)){
					std::cout<<"sensor detected:right"<<std::endl;
					break;
				} else {
					turnRight();
					usleep(STEP_TIME);
					moveForward();
					usleep(STEP_TIME);
				}
			} else if(routeDir.half==transDirection(Maze::DirLeft).half){
				std::cout<<"route:left"<<std::endl;
				if(senseLeft(realMaze)){
					std::cout<<"sensor detected:left"<<std::endl;
					break;
				} else {
					turnLeft();
					usleep(STEP_TIME);
					moveForward();
					usleep(STEP_TIME);
				}
			} else {
				std::cout<<"route:back"<<std::endl;
				turnRight();
				usleep(STEP_TIME);
				turnRight();
				usleep(STEP_TIME);
				moveForward();
				usleep(STEP_TIME);
			}
			index=getCoordIndex();
			coord=getCoord();
		}
	}
	senseAndSetWall(realMaze);
	return route;
}

void Mouse::getUnsearchedCellIndexesOnEstimatedShortestPath(std::vector<int>& v,int start,int goal) const{
	v.clear();
	Graph graph=Graph(*maze);
	graph.dijkstra(goal,start,true);
	const Route route=graph.getRoute(goal,start);
	std::cout<<"unsearched cells:"<<std::endl;
	for(Route::const_iterator it=route.begin();it!=route.end();it++){
		int index=(*it);
		if(index!=start&&index!=goal&&!maze->isSearchedCell(index)){
			v.push_back(index);
			std::cout<<index<<":"<<maze->getMazeData()[index].chk_wall.half<<std::endl;
		}
	}
}
