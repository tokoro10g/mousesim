#include <sys/time.h>

class StopWatch{
	private:
		double begin;
		double end;
		double get_dtime() const;
	public:
		StopWatch():begin(0.0),end(0.0){}
		~StopWatch(){}
		void start();
		void stop();
		void show() const;
};
