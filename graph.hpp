#pragma once

#include <vector>
#include "maze.hpp"

class Edge{
	public:
	int to;
	int cost;
	bool found;
	void setCost(int _cost){ cost=_cost; }
};

typedef std::vector<Edge> Edges;

class Node{
	private:
		int index;
		Edges edges;
		bool done;
		int cost;
		int from;
	public:
		Node(int _index,int _cost):index(_index),done(false),cost(_cost),from(-255){}
		~Node(){}
		bool operator<(const Node right) const{
			return cost<right.cost;
		}
		bool operator>(const Node right) const{
			return cost>right.cost;
		}

		int getIndex() const{ return index; }

		const Edges& getEdges() const{ return edges; }
		void addEdge(int to,int weight,bool found);
		void deleteEdge(int to);

		bool isDone() const{ return done; }
		void setDone(){ done=true; }

		int getCost() const{ return cost; }
		void setCost(int _cost){ cost=_cost; }

		int getFrom() const{ return from; }
		void setFrom(int _from){ from=_from; }
		
		void print_node() const;

		void setCostOfEdge(int to, int cost);
};

typedef std::vector<Node> Nodes;

typedef std::vector<int> Route;

class Graph{
	private:
		Nodes nodes;
		int w;
	public:
		Graph(int _w):w(_w){}
		Graph(const Maze& maze);
		~Graph(){}
		void addNode(int index,int cost);
		void addEdge(int v,int u,int weight,bool found);
		void addDirectionalEdge(int from,int to,int weight,bool found);
		void deleteEdge(int v,int u);
		void print_graph() const;
		void dijkstra(int start,int end,bool isSearching);
		const Route getRoute(int start,int end) const;
};
