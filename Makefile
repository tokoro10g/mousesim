CXX=g++
OBJS=graph.o maze.o mouse.o viewer.o stopwatch.o

all: adachitest

adachitest: adachitest.o $(OBJS)
	$(CXX) -o $@ adachitest.o $(OBJS)

adachitest.o: adachitest.cpp
	$(CXX) -c -g -Wall adachitest.cpp
graph.o: graph.cpp
	$(CXX) -c -g -Wall graph.cpp
maze.o: maze.cpp
	$(CXX) -c -g -Wall maze.cpp
mouse.o: mouse.cpp
	$(CXX) -c -g -Wall mouse.cpp
viewer.o: viewer.cpp
	$(CXX) -c -g -Wall viewer.cpp
stopwatch.o: stopwatch.cpp
	$(CXX) -c -g -Wall stopwatch.cpp

clean:
	rm -f *.o *.out
