#include <iostream>
#include "stopwatch.hpp"

void StopWatch::start(){
	begin=get_dtime();
}

void StopWatch::stop(){
	end=get_dtime();
}

void StopWatch::show() const{
	std::cout<<"<StopWatch>: "<<(end-begin)*1000<<"ms"<<std::endl;
}

double StopWatch::get_dtime() const{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return ((double)(tv.tv_sec) + (double)(tv.tv_usec) * 0.001 * 0.001);
}
