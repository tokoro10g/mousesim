MouseSim
========

## An experimental simulator for the development of a micromouse

This application visualizes an algorithm to search and solve a maze.
To enhance the usability for the micromouse, the algorithm implemented is based on Adachi-Method and Dijkstra's shortest path algorithm.

Usage
-----
* Build the project.

    ```
    $ make
    ```

* Run

    ```
    $ ./adachitest < ./mazedat/maze.dat
    ```

About Maze Data
---------------
The format of maze data is below:
    ```
    (Reserved)
    (Width of the maze)
    (Height of the maze)
    (Width x Height of Hex data)
    ```

