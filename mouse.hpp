#include <vector>
#include <stdint.h>
#include "graph.hpp"
#include "maze.hpp"
#include "viewer.hpp"

#define FRONTD(x) (x)
#define RIGHTD(x) ((((x&0x8)>>3)|(x<<1))&0xF)
#define LEFTD(x) ((((x&0x1)<<3)|(x>>1))&0xF)

class Mouse{
	private:
		Coord c;
		Maze *maze;
		uint8_t w;
	public:
		static const long STEP_TIME=100000;
	protected:
		static const char *dirchr;
	public:
		Mouse(const Maze& newmaze,int width);
		~Mouse(){ delete maze; }
		void senseAndSetWall(const Maze& realMaze);
		bool senseFront(const Maze& realMaze) const;
		bool senseRight(const Maze& realMaze) const;
		bool senseLeft(const Maze& realMaze) const;
		bool moveForward();
		void turnRight();
		void turnLeft();
		const Coord getCoord() const{ return c; }
		const int getCoordIndex() const{ return c.y*w+c.x; }
		const Maze& getMaze() const{ return *(maze); }
		void setMaze(const Maze& newmaze);
		void setWall(Direction dir);
		void clearMaze();
		Direction transDirection(const Direction local) const;
		const Route searchMaze(Viewer* viewer,Maze& realMaze,int maze_w,int start,int goal,bool isSearching);
		void getUnsearchedCellIndexesOnEstimatedShortestPath(std::vector<int>& v,int start,int goal) const;
};
