#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <cstdio>
#include "graph.hpp"
#include "maze.hpp"

void Node::addEdge(int to,int weight,bool found){
	Edge edge;
	edge.to=to;
	edge.cost=weight;
	edge.found=found;
	edges.push_back(edge);
}

void Node::deleteEdge(int to){
	for(Edges::iterator it=edges.begin();it!=edges.end();){
		if((*it).to==to){
			it=edges.erase(it);
			continue;
		}
		it++;
	}
}

void Node::setCostOfEdge(int to,int cost){
	for(Edges::iterator it=edges.begin();it!=edges.end();it++){
		if((*it).to==to&&(*it).cost<cost){
			(*it).setCost(cost);	
		}
	}
}

void Node::print_node() const{
	printf("index:%d, done:%s, cost:%d, from:%d\nedges:\n",
		index,done?"true":"false",cost,from);
	for(Edges::const_iterator it=edges.begin();it!=edges.end();it++){
		std::cout<<"cost:"<<(*it).cost<<", to:"<<(*it).to<<std::endl;
	}
}

Graph::Graph(const Maze& maze){
	w=maze.getWidth();
	int cnt=0;
	const MazeData& md=maze.getMazeData();
	for(MazeData::const_iterator it=md.begin();it!=md.end();it++){
		addNode(cnt,-1);
		int cost=1;
		if(!((*it).wall.bits.NORTH)){
			if((*it).wall.bits.SOUTH){
				cost+=2;
			}
			if(cnt-w>=0&&md[cnt-w].wall.bits.NORTH){
				cost+=2;
			}
			nodes[cnt-w].setCostOfEdge(cnt,cost);
			addDirectionalEdge(cnt,cnt-w,cost,(*it).chk_wall.bits.NORTH);
		}
		cost=1;
		if(!((*it).wall.bits.EAST)){
			if((*it).wall.bits.WEST){
				cost+=2;
			}
			if(cnt+1<256&&md[cnt+1].wall.bits.EAST){
				cost+=2;
			}
			addDirectionalEdge(cnt,cnt+1,cost,(*it).chk_wall.bits.EAST);
		}
		cost=1;
		if(!((*it).wall.bits.SOUTH)){
			if((*it).wall.bits.NORTH){
				cost+=2;
			}
			if(cnt+w<256&&md[cnt+w].wall.bits.SOUTH){
				cost+=2;
			}
			addDirectionalEdge(cnt,cnt+w,cost,(*it).chk_wall.bits.SOUTH);
		}
		cost=1;
		if(!((*it).wall.bits.WEST)){
			if((*it).wall.bits.EAST){
				cost+=2;
			}
			if(cnt-1>=0&&md[cnt-1].wall.bits.WEST){
				cost+=2;
 			}
			nodes[cnt-1].setCostOfEdge(cnt,cost);
			addDirectionalEdge(cnt,cnt-1,cost,(*it).chk_wall.bits.WEST);
		}
		cnt++;
	}
}

void Graph::addNode(const int index,const int cost){
	Node node(index,cost);
	nodes.push_back(node);
}

void Graph::addEdge(int v,int u,int weight,bool found){
	nodes[u].addEdge(v,weight,found);
	nodes[v].addEdge(u,weight,found);
}

void Graph::addDirectionalEdge(int from,int to,int weight,bool found){
	nodes[from].addEdge(to,weight,found);
}

void Graph::deleteEdge(int v,int u){
	nodes[u].deleteEdge(v);
	nodes[v].deleteEdge(u);
}

void Graph::print_graph() const{
	int cursor=0;
	for(Nodes::const_iterator it=nodes.begin();it!=nodes.end();it++){
		if((*it).isDone()){
			std::cout<<"*";
		} else {
			std::cout<<"o";
		}
		cursor++;
		if(cursor==w){
			std::cout<<std::endl;
			cursor=0;
		}
	}
}

void Graph::dijkstra(int start,int end,bool isSearching){
	nodes[start].setCost(0);

	std::priority_queue<Node *> q;
	std::vector<int> in_queue;
	q.push(&nodes[start]);

	while(!q.empty()){
		Node *doneNode=q.top();
		q.pop();
		for(std::vector<int>::iterator it=in_queue.begin();it!=in_queue.end();){
			if((*it)==doneNode->getIndex()){
				it=in_queue.erase(it);
				continue;
			}
			it++;
		}
		doneNode->setDone();
		Edges edges=doneNode->getEdges();
		for(Edges::iterator it=edges.begin();it!=edges.end();it++){
			int to=(*it).to;
			int cost=doneNode->getCost()+(*it).cost;
			if(isSearching||(*it).found){
				if(nodes[to].getCost()<0||cost<nodes[to].getCost()){
					nodes[to].setCost(cost);
					nodes[to].setFrom(doneNode->getIndex());
					if(find(in_queue.begin(),in_queue.end(),to)==in_queue.end()){
						q.push(&nodes[to]);
						in_queue.push_back(to);
					}
				}
			}
		}
	}
}

const Route Graph::getRoute(int start,int end) const{
	int cursor=end;
	int diff=0;
	Route route;

	while(cursor>=0&&cursor!=start){
			route.push_back(cursor);
		if(!nodes[cursor].isDone()||diff!=cursor-nodes[cursor].getFrom()){
			diff=cursor-nodes[cursor].getFrom();
		}
		cursor=nodes[cursor].getFrom();
	}
	route.push_back(start);

	return route;
}
